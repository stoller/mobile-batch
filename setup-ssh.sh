#!/bin/sh

SCRIPTNAME=$0

# Ignore

#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi
HOMEDIR="/users/$USER"
SSHDIR="$HOMEDIR/.ssh"
SSLDIR="$HOMEDIR/.ssl"

# Geni key to create an SSH key pair.
if [ ! -e $SSHDIR/id_rsa ]; then
    geni-get key > $SSHDIR/id_rsa
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get key failed!"
	exit 1
    fi
    chmod 600 $SSHDIR/id_rsa
fi

if [ ! -e $SSHDIR/id_rsa.pub ]; then
    ssh-keygen -y -f $SSHDIR/id_rsa > $SSHDIR/id_rsa.pub
    if [ $? -ne 0 ]; then
	echo "ERROR: ssh-keygen failed!"
	exit 1
    fi
    cat $SSHDIR/id_rsa.pub >> $SSHDIR/authorized_keys 
fi

# And we need an SSL certificate to use the portal tools
if [ ! -e $SSLDIR/emulab.pem ]; then
    if [ ! -e $SSLDIR ]; then
	mkdir $SSLDIR
	if [ $? -ne 0 ]; then
	    echo "ERROR: mkdir $SSLDIR failed!"
	    exit 1
	fi
	chmod 700 $SSLDIR
    fi
    geni-get rpccert > $SSLDIR/emulab.pem
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get rpccert failed!"
	exit 1
    fi
    chmod 600 $SSLDIR/emulab.pem
fi

exit 0;
