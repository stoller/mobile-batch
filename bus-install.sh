#!/bin/sh

sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

#
# Install the portal tools code. Needed in case the B210 is wonky
# and we need to power cycle ourself.
#
/local/repository/portal-tools/install.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/portal-tools/install.sh failed'
    exit 1
fi

#
# Install the monitor code.
#
/local/repository/monitor-spectrum/install.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/monitor-spectrum/install.sh failed'
    exit 1
fi

#
# Setup ssh between the nodes.
#
/local/repository/setup-ssh.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/setup-ssh.sh failed'
    exit 1
fi

exit 0
