"""Mobile batch demo. 
"""

# Ignore

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Route specific extensions.
import geni.rspec.emulab.route as route

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Maybe not needed.
OSIMAGE = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD'

#
# For testing just allocate some random nodes and make them do something.
#
TESTING = True
TESTINSTALL = 'sudo /bin/bash /local/repository/test-install.sh'

#
# Install script from the repository. This installs the uhd tools,
# gnuradio, etc. on the buses.
#
BUSINSTALL = 'sudo /bin/bash /local/repository/bus-install.sh'

#
# The controller needs the portal tools and the scripting that
# "drives" the buses. 
#
CTRLINSTALL = 'sudo /bin/bash /local/repository/ctrl-install.sh'
CTRLRUN     = '/local/repository/run.pl'

def findName(list, key):
    for pair in list:
        if pair[0] == key:
            return pair[1]
        pass
    return None

# Individual buses (for testing).
buses = [
    ('', "None"),
    ('urn:publicid:IDN+bus-4208.powderwireless.net+authority+cm',
     "Bus 4208"),
    ('urn:publicid:IDN+bus-4329.powderwireless.net+authority+cm',
     "Bus 4329"),
    ('urn:publicid:IDN+bus-4330.powderwireless.net+authority+cm',
     "Bus 4330"),
    ('urn:publicid:IDN+bus-4407.powderwireless.net+authority+cm',
     "Bus 4407"),
    ('urn:publicid:IDN+bus-4408.powderwireless.net+authority+cm',
     "Bus 4408"),
    ('urn:publicid:IDN+bus-4409.powderwireless.net+authority+cm',
     "Bus 4409"),
    ('urn:publicid:IDN+bus-4410.powderwireless.net+authority+cm',
     "Bus 4410"),
    ('urn:publicid:IDN+bus-4555.powderwireless.net+authority+cm',
     "Bus 4555"),
    ('urn:publicid:IDN+bus-4603.powderwireless.net+authority+cm',
     "Bus 4603"),
    ('urn:publicid:IDN+bus-4604.powderwireless.net+authority+cm',
     "Bus 4604"),
    ('urn:publicid:IDN+bus-4734.powderwireless.net+authority+cm',
     "Bus 4734"),
    ('urn:publicid:IDN+bus-4817.powderwireless.net+authority+cm',
     "Bus 4817"),
    ('urn:publicid:IDN+bus-4964.powderwireless.net+authority+cm',
     "Bus 4964"),
    ('urn:publicid:IDN+bus-5175.powderwireless.net+authority+cm',
     "Bus 5175"),
    ('urn:publicid:IDN+bus-6180.powderwireless.net+authority+cm',
     "Bus 6180"),
    ('urn:publicid:IDN+bus-6181.powderwireless.net+authority+cm',
     "Bus 6181"),
    ('urn:publicid:IDN+bus-6182.powderwireless.net+authority+cm',
     "Bus 6182"),
    ('urn:publicid:IDN+bus-6183.powderwireless.net+authority+cm',
     "Bus 6183"),
    ('urn:publicid:IDN+bus-6185.powderwireless.net+authority+cm',
     "Bus 6185"),
    ('urn:publicid:IDN+bus-6186.powderwireless.net+authority+cm',
     "Bus 6186")]

if TESTING:
    # Request a specific bus
    pc.defineParameter("Buses", "Bus",
                       portal.ParameterType.STRING, [buses[0][0]], buses,
                       min=1, multiValue=1, itemDefaultValue='')

    # Retrieve the values the user specifies during instantiation.
    params = pc.bindParameters()
    pass

#
# Request all bus routes. On a typical day, 4-6 buses are running across
# a few common routes. Morning is when the most buses run.
#
if TESTING:
    for bus in params.Buses:
        if bus != "":
            name = findName(buses, bus).replace(" ", "-")
            node = request.RawPC(name)
            node.component_id = "ed1"
            node.component_manager_id = bus
            node.disk_image = OSIMAGE
            node.addService(pg.Execute(shell="sh", command=TESTINSTALL))
            node.startVNC()
            pass
        pass
else:
    allroutes = request.requestAllRoutes()
    allroutes.disk_image = OSIMAGE
    allroutes.addService(pg.Execute(shell="sh", command=BUSINSTALL))
    allroutes.startVNC()
    pass

#
# We also need a node to act as the controller of the experiment.
#
ctrl = request.RawPC("controller")
ctrl.disk_image = OSIMAGE;
ctrl.addService(pg.Execute(shell="sh", command=CTRLINSTALL))
ctrl.startVNC()

portal.context.printRequestRSpec()
