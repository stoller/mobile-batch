#!/bin/sh

sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

sudo apt-get -y install --no-install-recommends libxml-libxml-perl libjson-perl
if [ $? -ne 0 ]; then
    echo 'apt-get install libxml-libxml-perl failed'
    exit 1
fi

#
# Install the portal tools code.
#
/local/repository/portal-tools/install.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/portal-tools/install.sh failed'
    exit 1
fi

#
# Setup ssh between the nodes.
#
/local/repository/setup-ssh.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/setup-ssh.sh failed'
    exit 1
fi

exit 0
